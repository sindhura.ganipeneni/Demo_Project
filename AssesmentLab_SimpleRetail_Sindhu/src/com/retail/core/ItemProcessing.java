package com.retail.core;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ItemProcessing implements Comparable<Item> {
Item item1;
public boolean isValidShippingMethod(String shipping)
{
	if((shipping.equalsIgnoreCase("AIR"))||(shipping.equalsIgnoreCase("GROUND")))
	{
		return true;
	}
	return false;
}
public static void testDisplay(ArrayList<Item> items)
{
	for(Item item:items)
	{
		System.out.println(item.getUpc()+" "+item.getDescription()+" "+item.getPrice()+" "+item.getWeight()+" "+item.getShipMethod()+" "+item.getShippingCost());
	}
}
public int compareTo(Item item)
{
	long upc1=item1.getUpc();
	long upc2=item.getUpc();
	if(upc1<upc2)
	{
		return -1;
	}
	else if(upc1>upc2)
	{
		return 1;
	}
	else return 0;
}
public static Comparator<Item> comparator = new Comparator<Item>()
{
public int compare(Item item1,Item item2)
{
	long upc1=item1.getUpc();
	long upc2=item2.getUpc();
	if(upc1<upc2)
	{
		return -1;
	}
	else if(upc1>upc2)
	{
		return 1;
	}
	else return 0;
//return(int)(item1.getUpc()-item2.getUpc());
}
};
public static void calculateShippingCost(ArrayList<Item> items)
{
	for(Item item:items)
	{
		double amount=calculateCost(item);
		//System.out.println(amount);
		item.setShippingCost(amount);
	}
}
public static double calculateCost(Item item)
{
	if(item.getShipMethod().equalsIgnoreCase("AIR"))
	{
		int sec2last=(int)((item.getUpc()%100))/10;
		double cal=sec2last*item.getWeight();
		int act=(int) (cal*100);
		double val=(double)act/100;
		return val;
	}
	else if(item.getShipMethod().equalsIgnoreCase("GROUND"))
	{
		float calc=(float) (item.getWeight()*2.5);
		int act=(int) (calc*100);
		double val=(double)act/100;
		return val;
	}
	return 0;
}
public static double calculateTotal(List<Item> it)
{
	double total=0;
	for(Item items:it)
	{
		total+=items.getShippingCost();
	}
	return total;
}
}
